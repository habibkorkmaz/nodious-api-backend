-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Erstellungszeit: 17. Okt 2014 um 11:04
-- Server Version: 5.5.39
-- PHP-Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `nodious`
--
CREATE DATABASE IF NOT EXISTS `nodious` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `nodious`;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `fbuser`
--

CREATE TABLE IF NOT EXISTS `fbuser` (
`id` bigint(20) NOT NULL,
  `birthday` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `facebookId` varchar(255) DEFAULT NULL,
  `firstName` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `homeTownId` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `homeTownName` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `lastName` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `locale` varchar(255) DEFAULT NULL,
  `locationId` varchar(255) DEFAULT NULL,
  `locationName` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `timezone` int(11) NOT NULL,
  `updateTime` datetime DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `verified` bit(1) NOT NULL,
  `friendOf` varchar(255) DEFAULT NULL,
  `clientId` varchar(255) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=337 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `page`
--

CREATE TABLE IF NOT EXISTS `page` (
`id` bigint(20) NOT NULL,
  `accept_charset` varchar(255) DEFAULT NULL,
  `client` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `page_duration` bigint(20) DEFAULT NULL,
  `page_url` varchar(255) DEFAULT NULL,
  `referrer` varchar(255) DEFAULT NULL,
  `session` varchar(255) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `user_cookie` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `fbuser`
--
ALTER TABLE `fbuser`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `fbuser`
--
ALTER TABLE `fbuser`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=337;
--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;







ALTER TABLE ios_contact CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE ios_contact_address CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE ios_contact_email CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE ios_contact_ims CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE ios_contact_ios_contact_email CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE ios_contact_list CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE ios_event CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE ios_session CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE ios_contact_related_name CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE ios_fb_profile CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE ios_fb_friend CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;

