package com.oredata.nodious.server.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity(name = "page")
@Table(name = "page")
@NamedQueries({ 
	@NamedQuery(name = "get.by.hostname", query = "select distinct p from page p where p.host=:host order by p.created"),
	@NamedQuery(name = "get.previous.page", query = "select distinct p from page p where p.host=:host order by p.created desc"),
	@NamedQuery(name = "get.by.userId", query = "select distinct p from page p where p.userId=:userId order by p.created"),
	@NamedQuery(name = "get.by.client", query = "select distinct p from page p where p.client=:client order by p.created")
})
public class PageImpl implements Page {

	@Id
	@GeneratedValue
	private long id;
	
	private String client;
	
	private String session;
	
	@Column(name = "user_id")
	private String userId;
	
	@Column(name = "user_cookie")
	private String userCookie;
	
	private Date created;
	
	@Column(name = "page_url")
	private String pageUrl;
	
	private String host;
	
	private String referrer;
	
	@Column(name = "page_duration")
	private long pageDuration;
	
	private String ip;
	
	@Column(name = "user_agent")
	private String userAgent;
	
	@Column(name = "accept_charset")
	private String acceptCharset;
	
	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.Page#getId()
	 */
	@Override
	public long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.Page#setId(long)
	 */
	@Override
	public void setId(long id) {
		this.id = id;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Date getCreated() {
		return created;
	}
	

	public String getUserCookie() {
		return userCookie;
	}

	public void setUserCookie(String userCookie) {
		this.userCookie = userCookie;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.Page#getPageUrl()
	 */
	@Override
	public String getPageUrl() {
		return pageUrl;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.Page#setPageUrl(java.lang.String)
	 */
	@Override
	public void setPageUrl(String pageUrl) {
		this.pageUrl = pageUrl;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.Page#getHost()
	 */
	@Override
	public String getHost() {
		return host;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.Page#setHost(java.lang.String)
	 */
	@Override
	public void setHost(String host) {
		this.host = host;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.Page#getReferrer()
	 */
	@Override
	public String getReferrer() {
		return referrer;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.Page#setReferrer(java.lang.String)
	 */
	@Override
	public void setReferrer(String referrer) {
		this.referrer = referrer;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.Page#getPageDuration()
	 */
	@Override
	public long getPageDuration() {
		return pageDuration;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.Page#setPageDuration(long)
	 */
	@Override
	public void setPageDuration(long pageDuration) {
		this.pageDuration = pageDuration;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.Page#getIp()
	 */
	@Override
	public String getIp() {
		return ip;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.Page#setIp(java.lang.String)
	 */
	@Override
	public void setIp(String ip) {
		this.ip = ip;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.Page#getUserAgent()
	 */
	@Override
	public String getUserAgent() {
		return userAgent;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.Page#setUserAgent(java.lang.String)
	 */
	@Override
	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.Page#getAcceptCharset()
	 */
	@Override
	public String getAcceptCharset() {
		return acceptCharset;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.Page#setAcceptCharset(java.lang.String)
	 */
	@Override
	public void setAcceptCharset(String acceptCharset) {
		this.acceptCharset = acceptCharset;
	}

	public String getSession() {
		return session;
	}

	public void setSession(String session) {
		this.session = session;
	}
}
