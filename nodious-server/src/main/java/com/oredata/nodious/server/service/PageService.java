package com.oredata.nodious.server.service;

public interface PageService {

	public void savePageInformation(String client, String pageUrl,
			String host, String session, String userId,String userCookie, String referrer, String pageDuration,
			String ip, String userAgent, String acceptCharset);
}
