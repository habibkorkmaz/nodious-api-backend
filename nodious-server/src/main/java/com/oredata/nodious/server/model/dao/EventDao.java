package com.oredata.nodious.server.model.dao;

import com.oredata.nodious.server.model.Event;

public interface EventDao {
	public void saveOrUpdate(Event event);
}
