package com.oredata.nodious.server.model.dao;

import com.oredata.nodious.server.model.Session;

public interface SessionDao {
	public void saveOrUpdate(Session session);
}
