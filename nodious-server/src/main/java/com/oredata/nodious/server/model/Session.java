package com.oredata.nodious.server.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.google.gson.annotations.SerializedName;

import flexjson.JSON;

@Entity
@Table(name = "ios_session")
public class Session  extends IosBase{
	@SerializedName("session_start")
	private Date sessionStart;

	@SerializedName("last_activity")
	private Date lastActivity;

	@SerializedName("active_time")
	private Integer activeTime;
	
	@SerializedName("session_length")
	private Integer sessionLength;
	
	public Date getSessionStart() {
		return sessionStart;
	}

	public void setSessionStart(Date sessionStart) {
		this.sessionStart = sessionStart;
	}

	public Date getLastActivity() {
		return lastActivity;
	}

	public void setLastActivity(Date lastActivity) {
		this.lastActivity = lastActivity;
	}

	public Integer getActiveTime() {
		return activeTime;
	}

	public void setActiveTime(Integer activeTime) {
		this.activeTime = activeTime;
	}

	public Integer getSessionLength() {
		return sessionLength;
	}

	public void setSessionLength(Integer sessionLength) {
		this.sessionLength = sessionLength;
	}

	@Override
	public String toString() {
		return "Session [uuid=" + uuid + ", createdAt="
				+ createdAt + ", sessionStart=" + sessionStart
				+ ", lastActivity=" + lastActivity + ", activeTime="
				+ activeTime + ", sessionLength=" + sessionLength
				+ ", sessionNo=" + sessionNo + "]";
	}

}
