package com.oredata.nodious.server.util;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class Utils {

	public static int createFiveDigitCode() {
		int meetingCode = (int) Math.floor(Math.random() * 10000 + 1);
		while ((int) (meetingCode / 100) < 10) {
			meetingCode = (int) Math.floor((Math.random()) * 10000 + 1);
		}
		return meetingCode;
	}

	public static String getDateTimeString(Date date) {
		SimpleDateFormat localDateTime = new SimpleDateFormat(
				"dd.MM.yyyy' 'HH:mm:ss");
		return localDateTime.format(date);
	}

	public static Date getDateFromString(String dateTime) {
		SimpleDateFormat localDateTime = new SimpleDateFormat(
				"dd.MM.yyyy' 'HH:mm:ss");

		Date date = null;
		try {
			date = localDateTime.parse(dateTime);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return date;
	}

	public static String createRandomString() {
		SecureRandom random = new SecureRandom();
		return new BigInteger(130, random).toString(32);
	}

	public static byte[] compress(String str) throws IOException {
        
        System.out.println("String length : " + str.length());
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        GZIPOutputStream gzip = new GZIPOutputStream(out);
        gzip.write(str.getBytes());
        gzip.close();
        String outStr = out.toString("UTF-8");
        System.out.println("Output String lenght : " + outStr.length());
        return out.toByteArray();
     }
    
    public static String decompress(byte[] str) throws IOException {
//        if (str == null || str.length() == 0) {
//            return str;
//        }
        
//        System.out.println("Input String length : " + str.length());
        GZIPInputStream gis = new GZIPInputStream(new ByteArrayInputStream(str));
        BufferedReader bf = new BufferedReader(new InputStreamReader(gis, "UTF-8"));
        String outStr = "";
        String line;
        while ((line=bf.readLine())!=null) {
          outStr += line;
        }
        System.out.println("Output String lenght : " + outStr.length());
        return outStr;
     }
  
     public static void main(String[] args) throws IOException {
        
        String string = "1111111111122222222222";	
        System.out.println("after compress:");
        byte[] compressed = compress(string);
        System.out.println(compressed);
        System.out.println("after decompress:");
        String decomp = decompress(compressed);
        System.out.println(decomp);
 
      }
}
