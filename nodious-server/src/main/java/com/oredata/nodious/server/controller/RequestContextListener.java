package com.oredata.nodious.server.controller;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;

public class RequestContextListener implements ServletRequestListener
{
    /* (non-Javadoc)
     * @see javax.servlet.ServletRequestListener#requestDestroyed(javax.servlet.ServletRequestEvent)
     */
    @Override
    public void requestDestroyed(ServletRequestEvent servletRequestEvent)
    {
    }

    /* (non-Javadoc)
     * @see javax.servlet.ServletRequestListener#requestInitialized(javax.servlet.ServletRequestEvent)
     */
    @Override
    public void requestInitialized(ServletRequestEvent servletRequestEvent)
    {
    }
}
