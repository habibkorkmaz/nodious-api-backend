package com.oredata.nodious.server.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.google.gson.annotations.SerializedName;

@Entity
@Table(name = "ios_fb_friend_list")
public class FbFriendList extends IosBase{

	@SerializedName("fb_friends")
	@OneToMany(cascade=CascadeType.ALL)
	private List<FbFriend> fbFriends = new ArrayList<FbFriend>();

	public List<FbFriend> getFbFriends() {
		return fbFriends;
	}

	public void setFbFriends(List<FbFriend> fbFriends) {
		this.fbFriends = fbFriends;
	}
}
