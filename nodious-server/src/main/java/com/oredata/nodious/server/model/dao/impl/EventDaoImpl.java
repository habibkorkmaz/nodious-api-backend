package com.oredata.nodious.server.model.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.oredata.nodious.server.model.Event;
import com.oredata.nodious.server.model.dao.AbstractHibernateDao;
import com.oredata.nodious.server.model.dao.EventDao;

@Repository("eventDao")
@Transactional
public class EventDaoImpl extends AbstractHibernateDao implements EventDao {

	@Override
	public void saveOrUpdate(Event event) {
		getSession().saveOrUpdate(event);
	}
}
