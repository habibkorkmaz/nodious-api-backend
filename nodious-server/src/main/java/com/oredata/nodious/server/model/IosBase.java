package com.oredata.nodious.server.model;

import java.util.Date;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import com.google.gson.annotations.SerializedName;

@MappedSuperclass
public abstract class IosBase {
	@Id
	protected String uuid;
	
	@SerializedName("os_name")
	protected String osName;
	
	@SerializedName("system_Version")
	protected String systemVersion;
	
	@SerializedName("device_name")
	protected String deviceName;
	
	@SerializedName("device_type")
	protected String deviceType;
	
	@SerializedName("bundle_version")
	protected String bundleVersion;
	
	@SerializedName("bundle_id")
	protected String bundleId;
	
	@SerializedName("session_no")
	protected Integer sessionNo;
	
	@SerializedName("created_at")
	protected Date createdAt;
	
	protected String country;
	protected String language;
	
	@SerializedName("user_id")
	protected String userId;
	protected String email;

	@SerializedName("user_name")
	protected String userName;
	
	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getOsName() {
		return osName;
	}

	public void setOsName(String osName) {
		this.osName = osName;
	}

	public String getSystemVersion() {
		return systemVersion;
	}

	public void setSystemVersion(String systemVersion) {
		this.systemVersion = systemVersion;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getBundleVersion() {
		return bundleVersion;
	}

	public void setBundleVersion(String bundleVersion) {
		this.bundleVersion = bundleVersion;
	}

	public String getBundleId() {
		return bundleId;
	}

	public void setBundleId(String bundleId) {
		this.bundleId = bundleId;
	}

	public Integer getSessionNo() {
		return sessionNo;
	}

	public void setSessionNo(Integer sessionNo) {
		this.sessionNo = sessionNo;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}
