package com.oredata.nodious.server.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.FacebookProfile;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.stereotype.Service;

import com.oredata.nodious.server.model.FBUser;
import com.oredata.nodious.server.model.FBUserImpl;
import com.oredata.nodious.server.model.dao.FBUserDao;
import com.oredata.nodious.server.model.dto.FBUserDto;
import com.oredata.nodious.server.model.dto.FBUserDtoList;
import com.oredata.nodious.server.service.FBUserService;

@Service
public class FBUserServiceImpl implements FBUserService {

	@Autowired
	private FBUserDao fbUserDao; 
	
	@Override
	public void saveFBUserInformation(FBUserDtoList fbUserDtoList, String clientId) {
		 assembly(fbUserDtoList, clientId);
	}
	
	private FBUser assembly(FBUserDtoList fbUserList, String clientId) {
		FBUser fbUser = assembly(fbUserList.getMe(), null, clientId);
		
		fbUser.setClientId(clientId);
//		fbUserDao.save(fbUser);
		
		for(FBUserDto fbUserDto : fbUserList.getFriendlistExd()) {
			assembly(fbUserDto, fbUser, clientId);
		}
		return fbUser;
	}

	private FBUser assembly(FBUserDto fbUserDto, FBUser friend, String clientId) {
		FBUser fbUser = new FBUserImpl();
		fbUser.setBirthday(fbUserDto.getBirthday());
		fbUser.setEmail(fbUserDto.getEmail());
		fbUser.setFirstName(fbUserDto.getFirst_name());
		fbUser.setGender(fbUserDto.getGender());
		fbUser.setFriendOf(friend);
		if(fbUserDto.getHometown() != null) {
			fbUser.setHomeTownId(fbUserDto.getHometown().getId());
			fbUser.setHomeTownName(fbUserDto.getHometown().getName());
		}
		fbUser.setFacebookId(fbUserDto.getId());
		fbUser.setLastName(fbUserDto.getLast_name());
		fbUser.setLink(fbUserDto.getLink());
		fbUser.setLocale(fbUserDto.getLocale());
		
		if(fbUserDto.getLocation()!=null) {
			fbUser.setLocationId(fbUserDto.getLocation().getId());
			fbUser.setLocationName(fbUserDto.getLocation().getName());
		}
		fbUser.setName(fbUserDto.getName());
		fbUser.setTimezone(fbUserDto.getTimezone());
		fbUser.setUsername(fbUserDto.getUsername());
		fbUser.setVerified(fbUserDto.isVerified());
		fbUser.setClientId(clientId);
		
		fbUserDao.save(fbUser);
		return fbUser;
	}

	@Async
	public void saveFBUserFriendListFromAccessToken(String accessToken, String clientId) {
		
		System.out.println("Token : " + accessToken);
		try{
			Facebook facebook = new FacebookTemplate(accessToken);
			List<String> friendIds = facebook.friendOperations().getFriendIds();
			System.out.println("List Size " + friendIds.size());
			
			FacebookProfile currentProfile = facebook.userOperations().getUserProfile();
			FBUser currentUser = fbUserDao.getByFacebookId(currentProfile.getId(), clientId);
			
			if(currentUser == null){
				currentUser = this.profileToFBUser(currentProfile, clientId);
				fbUserDao.save(currentUser);
			}
			
			for(String facebookId : friendIds){
				FacebookProfile friendProfile = facebook.userOperations().getUserProfile(facebookId);
				FBUser friendUser = this.profileToFBUser(friendProfile, clientId); 
				
				friendUser.setFriendOf(currentUser);
				
				fbUserDao.save(friendUser);
			}
		}catch(Exception exc){
			exc.printStackTrace();
		}
		
	}
	
	private FBUser profileToFBUser(FacebookProfile profile, String clientId){
		FBUser fbUser = new FBUserImpl();
		
		fbUser.setBirthday(profile.getBirthday());
		fbUser.setEmail(profile.getEmail());
		fbUser.setFirstName(profile.getFirstName());
		fbUser.setGender(profile.getGender());
		
		if(profile.getHometown() != null){
			fbUser.setHomeTownId(profile.getHometown().getId());
			fbUser.setHomeTownName(profile.getHometown().getName());
		}
		
		fbUser.setFacebookId(profile.getId());
		fbUser.setLastName(profile.getLastName());
		fbUser.setLink(profile.getLink());
		
		fbUser.setLocale(profile.getLocale().getDisplayName());
		
		if(profile.getLocation() != null ){
			fbUser.setLocationId(profile.getLocation().getId());
			fbUser.setLocationName(profile.getLocation().getName());
		}
		
		fbUser.setName(profile.getName());
		if(profile.getTimezone() != null)
			fbUser.setTimezone(profile.getTimezone().intValue());
		fbUser.setUpdateTime(new Date());
		fbUser.setClientId(clientId);
		
		return fbUser;
	}
}
