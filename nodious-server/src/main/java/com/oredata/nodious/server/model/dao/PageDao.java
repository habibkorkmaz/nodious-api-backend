package com.oredata.nodious.server.model.dao;

import java.util.List;

import com.oredata.nodious.server.model.Page;

public interface PageDao {
	public void save(Page page);
	public List<Page> get(String host);
	public Page getPreviousPage(String host);
	public Page getOnce(String client);
	public void delete(Page page);
	public List<Page> getAllofUser(String userId);
	public List<Page> getAll(String session);
}
