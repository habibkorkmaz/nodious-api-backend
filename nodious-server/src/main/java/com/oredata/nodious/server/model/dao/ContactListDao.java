package com.oredata.nodious.server.model.dao;

import java.util.List;

import com.oredata.nodious.server.model.ContactList;
import com.oredata.nodious.server.model.Page;

public interface ContactListDao {
	public void saveOrUpdate(ContactList contactList);
}
