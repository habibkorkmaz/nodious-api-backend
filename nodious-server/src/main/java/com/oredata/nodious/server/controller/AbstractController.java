package com.oredata.nodious.server.controller;

import com.oredata.nodious.server.util.SecurityContextUtils;


public abstract class AbstractController {
	
//	@Autowired
//	private UserService userService;
//	
	protected String getPrincipal()
    {
        return SecurityContextUtils.getUserNameInContext();
    }
	
//	protected User getUser() {
//		return userService.getUser(Long.valueOf(getPrincipal()));
//	}
}
