package com.oredata.nodious.server.model;

import java.util.Date;

import org.codehaus.jackson.map.annotate.JsonDeserialize;

@JsonDeserialize(as=PageImpl.class)
public interface Page {

	public abstract long getId();

	public abstract void setId(long id);
	
	public String getClient();

	public void setClient(String client);
	
	public String getUserId();

	public void setUserId(String userId);
	
	public String getUserCookie();
	
	public void setUserCookie(String userCookie);
	
	public Date getCreated();
	
	public void setCreated(Date created);

	public abstract String getPageUrl();

	public abstract void setPageUrl(String pageUrl);

	public abstract String getHost();

	public abstract void setHost(String host);

	public abstract String getReferrer();

	public abstract void setReferrer(String referrer);

	public abstract long getPageDuration();

	public abstract void setPageDuration(long pageDuration);

	public abstract String getIp();

	public abstract void setIp(String ip);

	public abstract String getUserAgent();

	public abstract void setUserAgent(String userAgent);

	public abstract String getAcceptCharset();

	public abstract void setAcceptCharset(String acceptCharset);
	
	public String getSession();

	public void setSession(String session);

}