package com.oredata.nodious.server.model.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.oredata.nodious.server.model.Session;
import com.oredata.nodious.server.model.dao.AbstractHibernateDao;
import com.oredata.nodious.server.model.dao.SessionDao;

@Repository("sessionDao")
@Transactional
public class SessionDaoImpl extends AbstractHibernateDao implements SessionDao {

	@Override
	public void saveOrUpdate(Session session) {
		getSession().saveOrUpdate(session);
	}
}
