package com.oredata.nodious.server.model.dto;

import java.util.List;

public class FBUserDtoList {

	private FBUserDto me;
	
	private List<FBUserDto> friendlistExd;

	public FBUserDto getMe() {
		return me;
	}

	public void setMe(FBUserDto me) {
		this.me = me;
	}

	public List<FBUserDto> getFriendlistExd() {
		return friendlistExd;
	}

	public void setFriendlistExd(List<FBUserDto> friendlistExd) {
		this.friendlistExd = friendlistExd;
	}
	
	
}
