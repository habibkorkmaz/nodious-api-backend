package com.oredata.nodious.server.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.google.gson.annotations.SerializedName;

@Entity
@Table(name="ios_fb_profile")
public class FbProfile extends IosBase {
	
	@SerializedName("fb_id")
	private String fbId;
	
	@SerializedName("fb_name")
	private String fbName;
	
	@SerializedName("fb_first_name")
	private String fbFirstName;
	
	@SerializedName("fb_link")
	private String fb_link;
	
	@SerializedName("fb_last_name")
	private String fbLastName;
	
	@SerializedName("fb_gender")
	private String fbGender;
	
	@SerializedName("fb_locale")
	private String fbLocale;
	
	@SerializedName("fb_timezone")
	private Integer fbTimezone;
	
	@SerializedName("fb_updated_time")
	private Date fbUpdatedTime;
	
	@SerializedName("fb_verified")
	private Boolean fbVerified;
	
	@SerializedName("fb_email")
	private String fbEmail;

	public String getFbId() {
		return fbId;
	}

	public void setFbId(String fbId) {
		this.fbId = fbId;
	}

	public String getFbName() {
		return fbName;
	}

	public void setFbName(String fbName) {
		this.fbName = fbName;
	}

	public String getFbFirstName() {
		return fbFirstName;
	}

	public void setFbFirstName(String fbFirstName) {
		this.fbFirstName = fbFirstName;
	}

	public String getFb_link() {
		return fb_link;
	}

	public void setFb_link(String fb_link) {
		this.fb_link = fb_link;
	}

	public String getFbLastName() {
		return fbLastName;
	}

	public void setFbLastName(String fbLastName) {
		this.fbLastName = fbLastName;
	}

	public String getFbGender() {
		return fbGender;
	}

	public void setFbGender(String fbGender) {
		this.fbGender = fbGender;
	}

	public String getFbLocale() {
		return fbLocale;
	}

	public void setFbLocale(String fbLocale) {
		this.fbLocale = fbLocale;
	}

	public Integer getFbTimezone() {
		return fbTimezone;
	}

	public void setFbTimezone(Integer fbTimezone) {
		this.fbTimezone = fbTimezone;
	}

	public Date getFbUpdatedTime() {
		return fbUpdatedTime;
	}

	public void setFbUpdatedTime(Date fbUpdatedTime) {
		this.fbUpdatedTime = fbUpdatedTime;
	}

	public Boolean getFbVerified() {
		return fbVerified;
	}

	public void setFbVerified(Boolean fbVerified) {
		this.fbVerified = fbVerified;
	}

	public String getFbEmail() {
		return fbEmail;
	}

	public void setFbEmail(String fbEmail) {
		this.fbEmail = fbEmail;
	}

	@Override
	public String toString() {
		return "FbProfile [fbId=" + fbId + ", fbName=" + fbName
				+ ", fbFirstName=" + fbFirstName + ", fb_link=" + fb_link
				+ ", fbLastName=" + fbLastName + ", fbGender=" + fbGender
				+ ", fbLocale=" + fbLocale + ", fbTimezone=" + fbTimezone
				+ ", fbUpdatedTime=" + fbUpdatedTime + ", fbVerified="
				+ fbVerified + ", fbEmail=" + fbEmail + "]";
	}
}
