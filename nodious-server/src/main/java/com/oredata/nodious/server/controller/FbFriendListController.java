package com.oredata.nodious.server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.oredata.nodious.server.model.FbFriendList;
import com.oredata.nodious.server.model.dao.FbFriendListDao;

@Controller
public class FbFriendListController{
	
	@Autowired
	private FbFriendListDao fbFriendListDao;

	@RequestMapping(value = "/api/ios/fb_friends", method = RequestMethod.POST)
	public ResponseEntity<String> saveFbFriendList(@RequestBody String json, @RequestHeader("Api-Key") String apiKey, @RequestHeader("Client-Sdk") String clientSdk ) 
				throws Exception {
		try{
			
	        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
	        FbFriendList friendList = gson.fromJson(json, FbFriendList.class);
	        
			fbFriendListDao.saveOrUpdate(friendList);
			
			return new ResponseEntity<String>(HttpStatus.CREATED);
		}catch(Exception exc){
			exc.printStackTrace();
			return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
