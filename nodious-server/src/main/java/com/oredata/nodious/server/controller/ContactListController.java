package com.oredata.nodious.server.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.oredata.nodious.server.model.Contact;
import com.oredata.nodious.server.model.ContactList;
import com.oredata.nodious.server.model.dao.ContactListDao;

import flexjson.JSONDeserializer;
import flexjson.transformer.DateTransformer;

@Controller
public class ContactListController{
	
	@Autowired
	private ContactListDao contactListDao;

	@RequestMapping(value = "/api/ios/contacts", method = RequestMethod.POST)
	public ResponseEntity<String> saveContactList(@RequestBody String json, @RequestHeader("Api-Key") String apiKey, @RequestHeader("Client-Sdk") String clientSdk ) 
				throws Exception {
		try{
			
	        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
	        ContactList contacts = gson.fromJson(json, ContactList.class);
	        
			contactListDao.saveOrUpdate(contacts);
			
			return new ResponseEntity<String>(HttpStatus.CREATED);
		}catch(Exception exc){
			exc.printStackTrace();
			return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
