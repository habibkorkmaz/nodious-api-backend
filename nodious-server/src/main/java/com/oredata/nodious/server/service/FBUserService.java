package com.oredata.nodious.server.service;

import com.oredata.nodious.server.model.dto.FBUserDtoList;

public interface FBUserService {

	public void saveFBUserInformation(FBUserDtoList fbUserDtoList, String clientId);

	public void saveFBUserFriendListFromAccessToken(String accessToken, String clientId);
}
