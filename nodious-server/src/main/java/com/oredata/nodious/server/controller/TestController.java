package com.oredata.nodious.server.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class TestController {



	@RequestMapping(value = "/api/test", method = RequestMethod.GET)
	@ResponseBody
	public Object loginT( HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		response.setStatus(HttpServletResponse.SC_OK);
		return "What's up homie?";
	}
}
