package com.oredata.nodious.server.model.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.oredata.nodious.server.model.FbFriendList;
import com.oredata.nodious.server.model.FbProfile;
import com.oredata.nodious.server.model.dao.AbstractHibernateDao;
import com.oredata.nodious.server.model.dao.FbFriendListDao;
import com.oredata.nodious.server.model.dao.FbProfileDao;

@Repository("fbProfileDao")
@Transactional
public class FbProfileDaoImpl extends AbstractHibernateDao implements FbProfileDao {

	@Override
	public void saveOrUpdate(FbProfile fbProfile) {
		getSession().saveOrUpdate(fbProfile);
	}
}
