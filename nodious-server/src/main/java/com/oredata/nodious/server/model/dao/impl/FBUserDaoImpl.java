package com.oredata.nodious.server.model.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.oredata.nodious.server.model.FBUser;
import com.oredata.nodious.server.model.FBUserImpl;
import com.oredata.nodious.server.model.dao.AbstractHibernateDao;
import com.oredata.nodious.server.model.dao.FBUserDao;

@Repository
@Transactional
public class FBUserDaoImpl extends AbstractHibernateDao implements FBUserDao {

	@Override
	public void save(FBUser fbUser) {
		getSession().save(fbUser);
	}

	@Override
	public FBUser get(Long id) {
		return (FBUser) getSession().get(FBUserImpl.class, id);
	}

	@Override
	public void delete(FBUser fbUser) {
		getSession().delete(fbUser);
	}

	@Override
	public FBUser getByFacebookId(String facebookId, String clientId) {
		String sql = "from fbuser where facebookId=:facebookId and clientId=:clientId";
		List results =  getSession().createQuery(sql)
						.setParameter("facebookId", facebookId)
						.setParameter("clientId", clientId)
						.list();
		           
		if(results.size() > 0)
			return (FBUser) results.get(0);
		else
			return null;
	}

}
