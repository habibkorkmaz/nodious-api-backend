package com.oredata.nodious.server.model.dao;

import com.oredata.nodious.server.model.FbProfile;

public interface FbProfileDao {
	public void saveOrUpdate(FbProfile fbProfile);
}
