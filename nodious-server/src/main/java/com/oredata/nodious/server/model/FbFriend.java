package com.oredata.nodious.server.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.google.gson.annotations.SerializedName;

@Entity
@Table(name = "ios_fb_friend")
public class FbFriend {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@SerializedName("fb_id")
	private String fbId;
	
	@SerializedName("fb_name")
	private String fbName;
	
	@SerializedName("fb_first_name")
	private String fbFirstName;
	
	@SerializedName("fb_last_name")
	private String fbLastName;

	public String getFbId() {
		return fbId;
	}

	public void setFbId(String fbId) {
		this.fbId = fbId;
	}

	public String getFbName() {
		return fbName;
	}

	public void setFbName(String fbName) {
		this.fbName = fbName;
	}

	public String getFbFirstName() {
		return fbFirstName;
	}

	public void setFbFirstName(String fbFirstName) {
		this.fbFirstName = fbFirstName;
	}

	public String getFbLastName() {
		return fbLastName;
	}

	public void setFbLastName(String fbLastName) {
		this.fbLastName = fbLastName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
}
