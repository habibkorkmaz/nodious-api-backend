package com.oredata.nodious.server.model.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.oredata.nodious.server.model.Page;
import com.oredata.nodious.server.model.dao.AbstractHibernateDao;
import com.oredata.nodious.server.model.dao.PageDao;

@Repository
@Transactional
public class PageDaoImpl extends AbstractHibernateDao implements PageDao {

	@Override
	public void save(Page page) {
		getSession().saveOrUpdate(page);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Page> get(String host) {
		return getSession().getNamedQuery("get.by.hostname")
				.setString("host", host)
				.list();
	}
	
	@Override
	public Page getPreviousPage(String host) {
		return (Page) getSession().getNamedQuery("get.previous.page")
				.setString("host", host)
				.setMaxResults(1)
				.uniqueResult();
	}

	@Override
	public void delete(Page page) {
		getSession().delete(page);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Page> getAllofUser(String userId) {
		return getSession().getNamedQuery("get.by.userId")
				.setString("userId", userId)
				.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Page> getAll(String client) {
		return getSession().getNamedQuery("get.by.client")
				.setString("client", client)
				.list();
	}

	@Override
	public Page getOnce(String host) {
		return (Page) getSession().getNamedQuery("get.by.hostname")
				.setString("host", host)
				.setMaxResults(1).uniqueResult();
	}

}
