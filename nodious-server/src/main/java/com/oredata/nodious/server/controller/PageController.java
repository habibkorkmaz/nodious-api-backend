package com.oredata.nodious.server.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.oredata.nodious.server.service.PageService;

@Controller
public class PageController {

	@Autowired
	private PageService pageService;

	@RequestMapping(value = "/api/page", method = {RequestMethod.POST,RequestMethod.GET})
	@ResponseStatus(HttpStatus.CREATED)
	public void savePage(
			@RequestParam(value = "client", required = false) String client,
			@RequestParam(value = "url", required = false) String pageUrl,
			@RequestParam(value = "host") String host,
			@RequestParam(value = "session", required = false) String session,
			@RequestParam(value = "userId", required = false) String userId,
			@RequestParam(value = "userCookie", required = false) String userCookie,
			@RequestParam(value = "referrer", required = false) String referrer,
			@RequestParam(value = "page_duration", required = false) String pageDuration,
			@RequestParam(value = "ip", required = false) String ip,
			@RequestParam(value = "user_agent", required = false) String userAgent,
			@RequestParam(value = "accept_charset", required = false) String acceptCharset,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		pageService.savePageInformation(client, pageUrl, host, session, userId,
				userCookie, referrer, pageDuration, request.getRemoteAddr(), userAgent,
				acceptCharset);
	}
	
	
//	@RequestMapping(value = "/api/page", method = RequestMethod.GET)
//	@ResponseStatus(HttpStatus.CREATED)
//	public void savePageViaGet(
//			@RequestParam(value = "client", required = false) String client,
//			@RequestParam(value = "url", required = false) String pageUrl,
//			@RequestParam(value = "host") String host,
//			@RequestParam(value = "session", required = false) String session,
//			@RequestParam(value = "userId", required = false) String userId,
//			@RequestParam(value = "userCookie", required = false) String userCookie,
//			@RequestParam(value = "referrer", required = false) String referrer,
//			@RequestParam(value = "page_duration", required = false) String pageDuration,
//			@RequestParam(value = "ip", required = false) String ip,
//			@RequestParam(value = "user_agent", required = false) String userAgent,
//			@RequestParam(value = "accept_charset", required = false) String acceptCharset,
//			HttpServletRequest request, HttpServletResponse response)
//			throws Exception {
//
//		pageService.savePageInformation(client, pageUrl, host, session, userId,
//				userCookie, referrer, pageDuration, request.getRemoteAddr(), userAgent,
//				acceptCharset);
//	}
}
