package com.oredata.nodious.server.model.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.oredata.nodious.server.model.ContactList;
import com.oredata.nodious.server.model.dao.AbstractHibernateDao;
import com.oredata.nodious.server.model.dao.ContactListDao;

@Repository("contactListDao")
@Transactional
public class ContactListDaoImpl extends AbstractHibernateDao implements ContactListDao {

	@Override
	public void saveOrUpdate(ContactList contactList) {
		getSession().saveOrUpdate(contactList);
	}
}
