package com.oredata.nodious.server.model.dao;

import com.oredata.nodious.server.model.FbFriendList;

public interface FbFriendListDao {
	public void saveOrUpdate(FbFriendList fbFriendList);
}
