package com.oredata.nodious.server.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.google.gson.annotations.SerializedName;

import flexjson.JSON;

@Entity
@Table(name="ios_contact")
public class Contact {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@SerializedName("firstname")
	private String firstName;

	@SerializedName("middlename")
	private String middleName;

	@SerializedName("lastname")
	private String lastName;
	private String prefix;
	private String suffix;

	@SerializedName("nickname")
	private String nickName;
	private String birthday;
	private String company;

	@OneToMany(cascade=CascadeType.ALL)
	private List<Email> emails = new ArrayList<Email>();

	@OneToMany(cascade=CascadeType.ALL)
	private List<Phone> phones = new ArrayList<Contact.Phone>();
	
	@OneToMany(cascade=CascadeType.ALL)
	private List<Address> addresses = new ArrayList<Contact.Address>();

	@OneToMany(cascade=CascadeType.ALL)
	private List<Ims> ims = new ArrayList<Contact.Ims>();

	@SerializedName("social_profiles")
	@OneToMany(cascade=CascadeType.ALL)
	private List<SocialProfile> socialProfiles = new ArrayList<Contact.SocialProfile>();

	@SerializedName("related_names")
	@OneToMany(cascade=CascadeType.ALL)
	private List<RelatedName> relatedNames = new ArrayList<Contact.RelatedName>();

	@Entity
	@Table(name="ios_contact_email")
	public static class Email {
		@Id
		@GeneratedValue(strategy=GenerationType.AUTO)
		private Long id;

		private String label;
		private String email;

		public String getLabel() {
			return label;
		}

		public void setLabel(String label) {
			this.label = label;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		@Override
		public String toString() {
			return "Email [label=" + label + ", email=" + email + "]";
		}

	}
	@Entity
	@Table(name="ios_contact_phone")
	public static class Phone {
		@Id
		@GeneratedValue(strategy=GenerationType.AUTO)
		private Long id;

		private String label;
		private String phone;

		public String getLabel() {
			return label;
		}

		public void setLabel(String label) {
			this.label = label;
		}

		public String getPhone() {
			return phone;
		}

		public void setPhone(String phone) {
			this.phone = phone;
		}

		@Override
		public String toString() {
			return "Phone [label=" + label + ", phone=" + phone + "]";
		}

	}

	@Entity
	@Table(name="ios_contact_address")
	public static class Address {
		@Id
		@GeneratedValue(strategy=GenerationType.AUTO)
		private Long id;

		private String label;
		private String street;
		private String city;
		private String state;
		private String zip;
		private String country;
		private String cc;

		public String getLabel() {
			return label;
		}

		public void setLabel(String label) {
			this.label = label;
		}

		public String getStreet() {
			return street;
		}

		public void setStreet(String street) {
			this.street = street;
		}

		public String getCity() {
			return city;
		}

		public void setCity(String city) {
			this.city = city;
		}

		public String getState() {
			return state;
		}

		public void setState(String state) {
			this.state = state;
		}

		public String getZip() {
			return zip;
		}

		public void setZip(String zip) {
			this.zip = zip;
		}

		public String getCountry() {
			return country;
		}

		public void setCountry(String country) {
			this.country = country;
		}

		public String getCc() {
			return cc;
		}

		public void setCc(String cc) {
			this.cc = cc;
		}

		@Override
		public String toString() {
			return "Address [label=" + label + ", street=" + street + ", city="
					+ city + ", state=" + state + ", zip=" + zip + ", country="
					+ country + ", cc=" + cc + "]";
		}

	}
	
	@Entity
	@Table(name="ios_contact_ims")
	public static class Ims {
		@Id
		@GeneratedValue(strategy=GenerationType.AUTO)
		private Long id;

		private String label;
		private String username;

		public String getLabel() {
			return label;
		}

		public void setLabel(String label) {
			this.label = label;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		@Override
		public String toString() {
			return "Ims [label=" + label + ", username=" + username + "]";
		}

	}
	
	@Entity
	@Table(name="ios_contact_social_profile")
	public static class SocialProfile {
		
		@Id
		@GeneratedValue(strategy=GenerationType.AUTO)
		private Long id;

		private String label;
		private String url;
		private String username;
		private String userid;

		public String getLabel() {
			return label;
		}

		public void setLabel(String label) {
			this.label = label;
		}

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getUserid() {
			return userid;
		}

		public void setUserid(String userid) {
			this.userid = userid;
		}

		@Override
		public String toString() {
			return "SocialProfile [label=" + label + ", url=" + url
					+ ", username=" + username + ", userid=" + userid + "]";
		}

	}

	@Entity
	@Table(name="ios_contact_related_name")
	public static class RelatedName {
		@Id
		@GeneratedValue(strategy=GenerationType.AUTO)
		private Long id;
		
		private String label;
		private String name;

		public String getLabel() {
			return label;
		}

		public void setLabel(String label) {
			this.label = label;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		@Override
		public String toString() {
			return "RelatedName [label=" + label + ", name=" + name + "]";
		}

	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public List<Email> getEmails() {
		return emails;
	}

	public void setEmails(List<Email> emails) {
		this.emails = emails;
	}

	public List<Phone> getPhones() {
		return phones;
	}

	public void setPhones(List<Phone> phones) {
		this.phones = phones;
	}

	public List<Address> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}

	public List<Ims> getIms() {
		return ims;
	}

	public void setIms(List<Ims> ims) {
		this.ims = ims;
	}

	public List<SocialProfile> getSocialProfiles() {
		return socialProfiles;
	}

	public void setSocialProfiles(List<SocialProfile> socialProfiles) {
		this.socialProfiles = socialProfiles;
	}

	public List<RelatedName> getRelatedNames() {
		return relatedNames;
	}

	public void setRelatedNames(List<RelatedName> relatedNames) {
		this.relatedNames = relatedNames;
	}
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Contact [firstName=" + firstName + ", middleName=" + middleName
				+ ", lastName=" + lastName + ", prefix=" + prefix + ", suffix="
				+ suffix + ", nickName=" + nickName + ", birthday=" + birthday
				+ ", company=" + company + ", emails=" + emails + ", phones="
				+ phones + ", addresses=" + addresses + ", ims=" + ims
				+ ", socialProfiles=" + socialProfiles + ", relatedNames="
				+ relatedNames + "]";
	}

}
