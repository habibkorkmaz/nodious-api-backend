package com.oredata.nodious.server.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.oredata.nodious.server.model.dto.FBUserDtoList;
import com.oredata.nodious.server.service.FBUserService;

@Controller
public class FBUserController{
	
	@Autowired
	private FBUserService fbUserService;

	@RequestMapping(value = "/api/facebook/user", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public void saveFBUserList(@RequestParam("fbUserList") String fbUserDtoListStr,@RequestParam("clientId") String clientId,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		FBUserDtoList fbUserDtoList = mapper.readValue(fbUserDtoListStr, FBUserDtoList.class);
		
		fbUserService.saveFBUserInformation(fbUserDtoList, clientId);
	}
	
	
	@RequestMapping(value = "/api/facebook/user/friends", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	@Async
	public void saveFBUserFriendListFromAccessToken(@RequestParam("accessToken") String accessToken,@RequestParam(required=false, value="clientId") String clientId,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		fbUserService.saveFBUserFriendListFromAccessToken(accessToken, clientId);
	}  

}
