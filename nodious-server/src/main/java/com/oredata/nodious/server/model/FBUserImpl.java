package com.oredata.nodious.server.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name = "fbuser")
public class FBUserImpl implements FBUser {

	@Id
	@GeneratedValue
	private long id;
	
	private String facebookId;
	
	private String birthday;
	
	private String email;
	
	private String name;
	
	private String username;
	
	private String firstName;
	
	private String lastName;
	
	private String gender;
	
	private String homeTownId;
	
	private String homeTownName;
	
	private String link;
	
	private String locationId;
	
	private String locationName;
	
	private String locale;
	
	private int timezone;
	
	private Date updateTime;
	
	private boolean verified;
	
	private String clientId;
	
	@ManyToOne(targetEntity=FBUserImpl.class, cascade={CascadeType.ALL})
    @JoinColumn(name="friendOf")
	private FBUser friendOf;
	
	@OneToMany(targetEntity=FBUserImpl.class, mappedBy="friendOf")
    private Set<FBUser> friends = new HashSet<FBUser>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFacebookId() {
		return facebookId;
	}

	public void setFacebookId(String facebookId) {
		this.facebookId = facebookId;
	}


	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.FBUser#getBirthday()
	 */
	@Override
	public String getBirthday() {
		return birthday;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.FBUser#setBirthday(java.lang.String)
	 */
	@Override
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.FBUser#getEmail()
	 */
	@Override
	public String getEmail() {
		return email;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.FBUser#setEmail(java.lang.String)
	 */
	@Override
	public void setEmail(String email) {
		this.email = email;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.FBUser#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.FBUser#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.FBUser#getUsername()
	 */
	@Override
	public String getUsername() {
		return username;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.FBUser#setUsername(java.lang.String)
	 */
	@Override
	public void setUsername(String username) {
		this.username = username;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.FBUser#getFirstName()
	 */
	@Override
	public String getFirstName() {
		return firstName;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.FBUser#setFirstName(java.lang.String)
	 */
	@Override
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.FBUser#getLastName()
	 */
	@Override
	public String getLastName() {
		return lastName;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.FBUser#setLastName(java.lang.String)
	 */
	@Override
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.FBUser#getGender()
	 */
	@Override
	public String getGender() {
		return gender;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.FBUser#setGender(java.lang.String)
	 */
	@Override
	public void setGender(String gender) {
		this.gender = gender;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.FBUser#getHomeTownId()
	 */
	@Override
	public String getHomeTownId() {
		return homeTownId;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.FBUser#setHomeTownId(java.lang.String)
	 */
	@Override
	public void setHomeTownId(String homeTownId) {
		this.homeTownId = homeTownId;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.FBUser#getHomeTownName()
	 */
	@Override
	public String getHomeTownName() {
		return homeTownName;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.FBUser#setHomeTownName(java.lang.String)
	 */
	@Override
	public void setHomeTownName(String homeTownName) {
		this.homeTownName = homeTownName;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.FBUser#getLink()
	 */
	@Override
	public String getLink() {
		return link;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.FBUser#setLink(java.lang.String)
	 */
	@Override
	public void setLink(String link) {
		this.link = link;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.FBUser#getLocationId()
	 */
	@Override
	public String getLocationId() {
		return locationId;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.FBUser#setLocationId(java.lang.String)
	 */
	@Override
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.FBUser#getLocationName()
	 */
	@Override
	public String getLocationName() {
		return locationName;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.FBUser#setLocationName(java.lang.String)
	 */
	@Override
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.FBUser#getLocale()
	 */
	@Override
	public String getLocale() {
		return locale;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.FBUser#setLocale(java.lang.String)
	 */
	@Override
	public void setLocale(String locale) {
		this.locale = locale;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.FBUser#getTimezone()
	 */
	@Override
	public int getTimezone() {
		return timezone;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.FBUser#setTimezone(int)
	 */
	@Override
	public void setTimezone(int timezone) {
		this.timezone = timezone;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.FBUser#getUpdateTime()
	 */
	@Override
	public Date getUpdateTime() {
		return updateTime;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.FBUser#setUpdateTime(java.util.Date)
	 */
	@Override
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.FBUser#isVerified()
	 */
	@Override
	public boolean isVerified() {
		return verified;
	}

	/* (non-Javadoc)
	 * @see com.oredata.nodious.server.model.FBUser#setVerified(boolean)
	 */
	@Override
	public void setVerified(boolean verified) {
		this.verified = verified;
	}

	public FBUser getFriendOf() {
		return friendOf;
	}

	public void setFriendOf(FBUser friendOf) {
		this.friendOf = friendOf;
	}

	public Set<FBUser> getFriends() {
		return friends;
	}

	public void setFriends(Set<FBUser> friends) {
		this.friends = friends;
	}

	public String getClientId() {
		return this.clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	
	

}
