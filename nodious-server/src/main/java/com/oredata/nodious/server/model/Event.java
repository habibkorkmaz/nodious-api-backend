package com.oredata.nodious.server.model;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.google.gson.annotations.SerializedName;


@Entity
@Table(name = "ios_event")
public class Event extends IosBase{
	@SerializedName("event_type")
	private Integer eventType;
	
	@SerializedName("parameter_1")
	private String parameter1;
	
	@SerializedName("parameter_2")
	private String parameter2;
	
	@SerializedName("parameter_3")
	private String parameter3;

	@Transient
	@SerializedName("parameter_custom")
	private Map<String, Object> parameterCustom = new HashMap<String, Object>();
	
	private String parameterCustomStr;

	@SerializedName("name_custom")
	private String nameCustom;
	
	public Integer getEventType() {
		return eventType;
	}

	public void setEventType(Integer eventType) {
		this.eventType = eventType;
	}

	public String getParameter1() {
		return parameter1;
	}

	public void setParameter1(String parameter1) {
		this.parameter1 = parameter1;
	}

	public String getParameter2() {
		return parameter2;
	}

	public void setParameter2(String parameter2) {
		this.parameter2 = parameter2;
	}

	public String getParameter3() {
		return parameter3;
	}

	public void setParameter3(String parameter3) {
		this.parameter3 = parameter3;
	}


	public String getParameterCustomStr() {
		return parameterCustom.toString();
	}

	public void setParameterCustomStr(String parameterCustomStr) {
		this.parameterCustomStr = parameterCustom.toString();
	}
	
	public Map<String, Object> getParameterCustom() {
		return parameterCustom;
	}

	public void setParameterCustom(Map<String, Object> parameterCustom) {
		this.parameterCustom = parameterCustom;
	}

	public String getNameCustom() {
		return nameCustom;
	}

	public void setNameCustom(String nameCustom) {
		this.nameCustom = nameCustom;
	}

}
