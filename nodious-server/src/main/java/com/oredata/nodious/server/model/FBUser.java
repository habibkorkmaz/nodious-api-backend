package com.oredata.nodious.server.model;

import java.util.Date;
import java.util.Set;

public interface FBUser {

	public long getId();

	public void setId(long id);

	public String getFacebookId();

	public void setFacebookId(String facebookId);

	public abstract String getBirthday();

	public abstract void setBirthday(String birthday);

	public abstract String getEmail();

	public abstract void setEmail(String email);

	public abstract String getName();

	public abstract void setName(String name);

	public abstract String getUsername();

	public abstract void setUsername(String username);

	public abstract String getFirstName();

	public abstract void setFirstName(String firstName);

	public abstract String getLastName();

	public abstract void setLastName(String lastName);

	public abstract String getGender();

	public abstract void setGender(String gender);

	public abstract String getHomeTownId();

	public abstract void setHomeTownId(String homeTownId);

	public abstract String getHomeTownName();

	public abstract void setHomeTownName(String homeTownName);

	public abstract String getLink();

	public abstract void setLink(String link);

	public abstract String getLocationId();

	public abstract void setLocationId(String locationId);

	public abstract String getLocationName();

	public abstract void setLocationName(String locationName);

	public abstract String getLocale();

	public abstract void setLocale(String locale);

	public abstract int getTimezone();

	public abstract void setTimezone(int timezone);

	public abstract Date getUpdateTime();

	public abstract void setUpdateTime(Date updateTime);

	public abstract boolean isVerified();

	public abstract void setVerified(boolean verified);
	
	public FBUser getFriendOf();
	public void setFriendOf(FBUser friendOf);
	public Set<FBUser> getFriends();
	public void setFriends(Set<FBUser> friends);
	
	public String getClientId();
	public void setClientId(String clientId);

}