package com.oredata.nodious.server.model.dao;

import com.oredata.nodious.server.model.FBUser;

public interface FBUserDao {
	public void save(FBUser fbUser);
	public FBUser get(Long id);
	public void delete(FBUser fbUser);
	public FBUser getByFacebookId(String facebookId, String clientId);
}
