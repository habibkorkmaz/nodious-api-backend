package com.oredata.nodious.server.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oredata.nodious.server.model.Page;
import com.oredata.nodious.server.model.PageImpl;
import com.oredata.nodious.server.model.dao.PageDao;
import com.oredata.nodious.server.service.PageService;
import com.oredata.nodious.server.util.Utils;

@Service
public class PageServiceImpl implements PageService {

	@Autowired
	private PageDao pageDao;
	
	@Override
	public void savePageInformation(String client, String pageUrl,
			String host, String session, String userId, String userCookie, String referrer,
			String pageDuration, String ip, String userAgent,
			String acceptCharset) {
		Page page = new PageImpl();
		page.setAcceptCharset(acceptCharset);
		page.setCreated(new Date());
		page.setHost(host);
		page.setSession(session);
		page.setIp(ip);
		page.setPageUrl(pageUrl);
		page.setReferrer(referrer);
		page.setUserAgent(userAgent);
		page.setUserCookie(userCookie);
		page.setUserId(userId);
		
		Page prevPage = null;
		if((prevPage =pageDao.getPreviousPage(host)) == null ) {
			page.setClient(Utils.createRandomString());
		} else {
			page.setClient(prevPage.getClient());
			if(prevPage.getSession().equals(page.getSession())) {
				long seconds = (page.getCreated().getTime()-prevPage.getCreated().getTime())/1000;
				prevPage.setPageDuration(seconds);
				pageDao.save(prevPage);
			}
		}
		
		pageDao.save(page);
	}
	

}
