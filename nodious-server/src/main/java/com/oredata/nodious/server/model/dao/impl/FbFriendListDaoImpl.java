package com.oredata.nodious.server.model.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.oredata.nodious.server.model.FbFriendList;
import com.oredata.nodious.server.model.dao.AbstractHibernateDao;
import com.oredata.nodious.server.model.dao.FbFriendListDao;

@Repository("fbFriendListDao")
@Transactional
public class FbFriendListDaoImpl extends AbstractHibernateDao implements FbFriendListDao {

	@Override
	public void saveOrUpdate(FbFriendList fbFriendList) {
		getSession().saveOrUpdate(fbFriendList);
	}
}
