package com.oredata.nodious.server.controller;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * If path paramter is available, check if path is valid. This filter
 * initializes the static request holder. The filter is called before other
 * spring security chain filter, because the request context is needed by
 * UserDetailsService to obtain user roles
 */
public class RequestContextFilter implements Filter {
	protected final Logger log = LoggerFactory.getLogger(getClass());

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.Filter#destroy()
	 */
	@Override
	public void destroy() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest,
	 * javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;

		 HttpServletResponse responseN = (HttpServletResponse) response;
		 responseN.setHeader("Access-Control-Allow-Origin", "*");
		 responseN.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
		 responseN.setHeader("Access-Control-Allow-Headers", "X-Requested-With");
	        
		log.info("##RequestContextFilter: " + httpRequest.getMethod() + "| "
				+ httpRequest.getRequestURI());

		chain.doFilter(request, response);
	}
}
