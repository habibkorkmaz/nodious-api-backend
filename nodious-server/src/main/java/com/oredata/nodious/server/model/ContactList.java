package com.oredata.nodious.server.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="ios_contact_list")
public class ContactList extends IosBase {

	@OneToMany(cascade=CascadeType.ALL)
	private List<Contact> contacts = new ArrayList<Contact>();
	

	public List<Contact> getContacts() {
		return contacts;
	}
	
	public void setContacts(List<Contact> contacts) {
		this.contacts = contacts;
	}

	@Override
	public String toString() {
		return "ContactList [uuid=" + uuid + ", createdAt=" + createdAt + ", contacts=" + contacts + "]";
	}
	
}
