package com.oredata.nodious.server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.oredata.nodious.server.model.Event;
import com.oredata.nodious.server.model.dao.EventDao;

@Controller
public class EventController{
	
	@Autowired
	private EventDao eventDao;

	@RequestMapping(value = "/api/ios/event	", method = RequestMethod.POST)
	public ResponseEntity<String> saveContactList(@RequestBody String json, @RequestHeader("Api-Key") String apiKey, @RequestHeader("Client-Sdk") String clientSdk) 
					throws Exception {
		try{
			
			Gson gson = new GsonBuilder()
					.setDateFormat("yyyy-MM-dd HH:mm:ss")
					.create();
			Event event = gson.fromJson(json, Event.class);
			
			event.setParameterCustomStr(event.getParameterCustom().toString());
			
			eventDao.saveOrUpdate(event);
			
			return new ResponseEntity<String>(HttpStatus.CREATED);
		}catch(Exception exc){
			exc.printStackTrace();
			return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
