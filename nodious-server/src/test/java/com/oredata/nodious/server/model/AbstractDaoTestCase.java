package com.oredata.nodious.server.model;


import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import com.oredata.nodious.server.model.dao.AbstractHibernateDao;

@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:conf/application-context.xml",
                                 "classpath:conf/datasource-context.xml"})
@TransactionConfiguration(transactionManager = "txManager",
                          defaultRollback = true)
public class AbstractDaoTestCase extends AbstractHibernateDao
{
    @Before
    public void beforeTest()
    {
    }

    @After
    public void afterTest()
    {
    }
}
