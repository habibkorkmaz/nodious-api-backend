package com.oredata.nodious.server.model;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Date;

import javax.transaction.Transactional;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.oredata.nodious.server.model.dao.PageDao;

@Transactional
public class PageDaoTest extends AbstractDaoTestCase {

	@Autowired
	private PageDao pageDao;
	
	private static final String CLIENT = "clientA";
	private static final String SESSION = "sessionA";
	private static final String HOST = "clientA.com";
	private static final String USER_COOKIE = "clientAXYZ";
	
	  @Override
	    public void beforeTest()
	    {
	        Page page = new PageImpl();
	        page.setAcceptCharset("dsadkasaksd");
	        page.setClient(CLIENT);
	        page.setCreated(new Date());
	        page.setHost(HOST);
	        page.setIp("192.168.2.1");
	        page.setPageUrl("www.clientA.com");
	        page.setSession(SESSION);
	        page.setUserCookie(USER_COOKIE);
	        
	 
	       
	        getSession().save(page);
	        getSession().flush();
	        getSession().evict(page);

	    }
	  
	  @Test
	  @Transactional
	    public void getPage()
	    {
	        
	        Page page = pageDao.getOnce(CLIENT);
	        assertNotNull(page);
	    
	    }
	  
	  
	  
	  @Test
	  @Transactional
	    public void deletePage()
	    {
		    Page page = pageDao.getOnce(CLIENT);
	        pageDao.delete(page);
	        
	        page = pageDao.getOnce(CLIENT);
	        assertNull(page);
	    
	    }
	    
	    
}
