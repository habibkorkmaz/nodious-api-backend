package com.oredata.nodious.server.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Test;

import com.oredata.nodious.server.util.Utils;


public class IosSdkControllerTest extends AbstractControllerTestCase {

	DefaultHttpClient httpclient = null;
	HttpPost httpost = null;
	
	private static String BASE_URL = "http://52.0.14.168/nodious-api";
	
    @Override
    public void beforeTest() throws Exception
    {
    	httpclient = new DefaultHttpClient();
    	httpost = new HttpPost();
        httpost.setHeader("Content-Type", "application/json");
        httpost.setHeader("Content-Encoding", "gzip");
        httpost.setHeader("Api-Key", "ApiKey123");
        httpost.setHeader("Client-Sdk", "Client-Sdk123");
    }

    @Override
    public void afterTest() throws Exception
    {
    }
    
    @Test
    public void addContactListTest() throws Exception
    {
        String foo = "{'uuid':'6a8780b5-d09a-4063-8fc2-1de0bfd29bff','os_name':'iOS','system_version':'8.1','device_name':'iPhone Simulator','device_type':'iPhone Simulator','bundle_version':'1','bundle_id':'com.nodious.NodiousExampleApp','session_no':1,'created_at':'2015-02-12 06:57:56','country':'US','language':'en','user_id':'1331351','email':'sosolakoglu@gmail.com', 'contacts': [{'firstname' : 'Sarp', 'middlename' : 'Ogulcan','lastname' : 'Solakoglu','prefix' : 'Mr.','suffix' : 'first','nickname' : 'sos','birthday' : '13/02/1991','company' : 'Oredata','emails' : [ { 'label':'home','email':'sosolakoglu@gmail.com'},{'label':'work','email':'sarp.solakoglu@oredata.com'},{'label':'work','email':'sarp@nodious.com'}],'phones': [{'label' : 'home','phone' : '+9021691201121'},{'label' : 'work','phone' : '+9021210311319'},{'label' : 'iPhone','phone' : '+9053633961303'}],'addresses': [{'label' : 'work','street' : 'Yıldız Teknokent C:207','city' : 'Davutpaşa','state' : 'İstanbul','zip' : '34501','country' : 'Turkey','cc' : 'tr'},{'label' : 'home','street' : 'Dalga Sok. Meltem Apt.','city' : 'Moda','state' : 'İstanbul','zip' : '34710','country' : 'Turkey','cc' : 'tr'}],'ims' : [{'label' : 'Skype','username' : 'sosolakoglu'},{'label' : 'MSN','username' : 'sarpsolakoglu'}],'social_profiles' : [{'label' : 'twitter','url' : 'http://twitter.com/sarpsolakoglu','username' : 'sarpsolakoglu','userid' : '38319391931'},{'label' : 'facebook','url' : 'http://www.facebook.com/solakoglu','username' : 'solakoglu','userid' : '13911401'}],'related_names' : [{'label' : 'mother','name' : 'Siren'},{'label' : 'father','name' : 'Can'},{'label' : 'friend','name' : 'İrem'}]},{'firstname':'Ömer','middlename':'Faruk','lastname':'Kurt','phones': [{'label' : 'home','phone' : '+9021612412567'},{'label' : 'work','phone' : '+9021210311319'}],'addresses': [{'label' : 'work','street' : 'Yıldız Teknokent C:207','city' : 'Davutpaşa','state' : 'İstanbul','zip' : '34501','country' : 'Turkey','cc' : 'tr'}]}]}";
        
        byte[] gzipped = Utils.compress(foo);
        HttpEntity entity = new ByteArrayEntity(gzipped);
        httpost.setEntity(entity);
        httpost.setURI( new URI( BASE_URL + "/api/ios/contacts"));
        
        HttpResponse response = httpclient.execute(httpost);
        
        System.out.println("Login form get: " + response.getStatusLine());
        if (entity != null) {
            entity.consumeContent();
        }

        httpclient.getConnectionManager().shutdown();      
    }
    
    
    @Test
    public void addSessionTest() throws Exception
    {
        String foo = "{'uuid':'6a8780b5-d09a-4063-8fc2-1de0bfd29bff','os_name':'iOS','system_version':'8.1','device_name':'iPhone Simulator','device_type':'iPhone Simulator','bundle_version':'1','bundle_id':'com.nodious.NodiousExampleApp','session_no':1,'created_at':'2015-02-12 06:57:56','country':'US','language':'en','user_id':'1331351','email':'sosolakoglu@gmail.com','session_start':'2015-02-12 06:57:56','last_activity':'2015-02-12 06:57:56','active_time':60,'session_length':100,'session_no':1}";
        
        byte[] gzipped = Utils.compress(foo);
        HttpEntity entity = new ByteArrayEntity(gzipped);
        httpost.setEntity(entity);
        httpost.setURI( new URI(BASE_URL + "/api/ios/session"));
        
        HttpResponse response = httpclient.execute(httpost);
        
        System.out.println("Login form get: " + response.getStatusLine());
        if (entity != null) {
            entity.consumeContent();
        }

        httpclient.getConnectionManager().shutdown();      
    }
    
    @Test
    public void addEventTest() throws Exception
    {
        //String foo = "{'uuid':'6a8780b5-d09a-4063-8fc2-1de0bfd29bff','os_name':'iOS','system_version':'8.1','device_name':'iPhone Simulator','device_type':'iPhone Simulator','bundle_version':'1','bundle_id':'com.nodious.NodiousExampleApp','session_no':1,'created_at':'2015-02-12 06:57:56','country':'US','language':'en','user_id':'1331351','email':'sosolakoglu@gmail.com','event_type':2,'parameter_1':10,'parameter_2':'US','parameter_3':'3jsakfa0atakb00'}";
    	String foo = "{'uuid':'6a8780b5-d09a-4063-8fc2-1de0bfd29bff','os_name':'iOS','system_version':'8.1','device_name':'iPhone Simulator','device_type':'iPhone Simulator','bundle_version':'1','bundle_id':'com.nodious.NodiousExampleApp','session_no':1,'created_at':'2015-02-12 06:57:56','country':'US','language':'en','user_id':'1331351','email':'sosolakoglu@gmail.com','event_type':11,'parameter_custom':{'asd':12,'sdf':23}}";
        
        byte[] gzipped = Utils.compress(foo);
        HttpEntity entity = new ByteArrayEntity(gzipped);
        httpost.setEntity(entity);
        httpost.setURI( new URI(BASE_URL+ "/api/ios/event"));
        
        HttpResponse response = httpclient.execute(httpost);
        
        System.out.println("Login form get: " + response.getStatusLine());
        if (entity != null) {
            entity.consumeContent();
        }

        httpclient.getConnectionManager().shutdown();      
    }
    
    @Test
    public void addFbProfileTest() throws Exception
    {
        String foo = "{'uuid':'6a8780b5-d09a-4063-8fc2-1de0bfd29bff','os_name':'iOS','system_version':'8.1','device_name':'iPhone Simulator','device_type':'iPhone Simulator','bundle_version':'1','bundle_id':'com.nodious.NodiousExampleApp','session_no':1,'created_at':'2015-02-12 06:57:56','country':'US','language':'en','user_id':'1331351','email':'sosolakoglu@gmail.com','fb_id':'10152593577523640','fb_name':'Sarp Solakoğlu','fb_first_name':'Sarp','fb_link':'https://www.facebook.com/app_scoped_user_id/10152593577523640/','fb_last_name':'Solakoğlu','fb_gender':'male','fb_locale':'en_US','fb_timezone': 2,'fb_updated_time': '2015-02-12 15:38:44','fb_verified': true,'fb_email': 'sosolakoglu@gmail.com'}"; 
        
        byte[] gzipped = Utils.compress(foo);
        HttpEntity entity = new ByteArrayEntity(gzipped);
        httpost.setEntity(entity);
        httpost.setURI( new URI( BASE_URL+"/api/ios/fb_profile"));
        
        HttpResponse response = httpclient.execute(httpost);
        
        System.out.println("Login form get: " + response.getStatusLine());
        if (entity != null) {
            entity.consumeContent();
        }

        httpclient.getConnectionManager().shutdown();      
    }
    
    @Test
    public void addFbFriendListTest() throws Exception
    {
        String foo = "{'uuid':'7a8780b5-d09a-4063-8fc2-1de0bfd29bff','os_name':'iOS','system_version':'8.1','device_name':'iPhone Simulator','device_type':'iPhone Simulator','bundle_version':'1','bundle_id':'com.nodious.NodiousExampleApp','session_no':1,'created_at':'2015-02-12 06:57:56','country':'US','language':'en','user_id':'1331351','email':'sosolakoglu@gmail.com','fb_friends': [{'fb_id':'10152593484992929','fb_name':'Ömer Kurt','fb_first_name':'Ömer','fb_last_name':'Kurt'},{'fb_id':'10152591399301931','fb_name':'Samet Özcan','fb_first_name':'Samet','fb_last_name':'Özcan'}]}"; 
        
        byte[] gzipped = Utils.compress(foo);
        HttpEntity entity = new ByteArrayEntity(gzipped);
        httpost.setEntity(entity);
        httpost.setURI( new URI( BASE_URL + "/api/ios/fb_friends"));
        
        HttpResponse response = httpclient.execute(httpost);
        
        System.out.println("Login form get: " + response.getStatusLine());
        if (entity != null) {
            entity.consumeContent();
        }

        httpclient.getConnectionManager().shutdown();      
    }

}
